﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class FlashOnHit : MonoBehaviour
{
    public Color FlashColor;
    public float FlashTime = 0.5f;
    private Color[] originalColors;

    private SkinnedMeshRenderer renderer;

    // Use this for initialization
    void Start()
    {
        renderer = GetComponentInChildren<SkinnedMeshRenderer>();

        originalColors = renderer.materials.Select(m => m.color).ToArray();
    }

    void OnDamage()
    {
        int no = renderer.materials.Length;
        for (int i = 0; i < no; i++)
        {
            renderer.materials[i].color = FlashColor;
        }

        Invoke("ResetColors", FlashTime);
    }

    void ResetColors()
    {
        int no = renderer.materials.Length;
        for (int i = 0; i < no; i++)
        {
            renderer.materials[i].color = originalColors[i];
        }
    }
}