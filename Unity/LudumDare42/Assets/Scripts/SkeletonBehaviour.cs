﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;

public class SkeletonBehaviour : MonoBehaviour
{
    public int Health = 3;
    public Transform SpawnPoint;
    public float WaitBetweenWalks = 3.0f;
    public float MovementSpeed = 20.0f;
    public float DeathTimer = 30f;

    public Transform ItemSlot;
    public GameObject SpawnStartingItem;


    private const string IsIdle = "IsIdle";
    private const string IsWalking = "IsWalking";
    private const string IsAttacking = "IsAttacking";
    private const string IsDeath = "IsDeath";

    private Rigidbody body;
    private NavMeshAgent agent;
    private Animator animations;
    private ItemController equipedItem = null;

    private float idleTimer = 0;

    enum states
    {
        Idle,
        Walking,
        Attacking,
        Death
    }

    private states currentState = states.Idle;


    // Use this for initialization
    void Start()
    {
        body = GetComponent<Rigidbody>();
        animations = GetComponentInChildren<Animator>();
        agent = GetComponent<NavMeshAgent>();

        if (SpawnPoint == null)
        {
            SpawnPoint = transform;
        }

        SetNewIdleWalkPoint();
        agent.speed = MovementSpeed;
        agent.stoppingDistance = 0.5f;

        if (SpawnStartingItem != null)
        {
            var o = GameObject.Instantiate(SpawnStartingItem);
            var i = o.GetComponent<ItemController>();
            i.SetAsCarried(ItemSlot);
            equipedItem = i;

            if (equipedItem.Type == ItemController.ItemType.Key)
            {
                SpawnPoint.GetComponent<SkyBeamController>().RegisterSkeleton();
            }
        }
    }

    private void FixedUpdate()
    {
        if (currentState == states.Death)
        {
            DeathTimer -= Time.deltaTime;
            if (DeathTimer <= 0)
            {
                GameObject.Destroy(gameObject);
            }

            return;
        }
        
        idleTimer -= Time.deltaTime;

        StateUpdate();
        SetAnimationState();
    }

    void StateUpdate()
    {
        switch (currentState)
        {
            case states.Idle:
                if (idleTimer <= 0)
                {
                    SetNewIdleWalkPoint();
                }

                break;
            case states.Walking:
                if (agent.remainingDistance <= 0.5)
                {
                    currentState = states.Idle;
                    idleTimer = WaitBetweenWalks;
                    // agent.isStopped = true;
                }

                break;
            case states.Attacking:
                break;
            case states.Death:
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    public void TakeDamage(Transform from, int damage, float power)
    {
        if (currentState == states.Death) return;
        SendMessage("OnDamage", SendMessageOptions.DontRequireReceiver);

        Health -= damage;
        if (Health <= 0)
        {
            OnDeath();
            return;
        }

        // Bump back.
//        var force = new Vector3(transform.position.x - from.position.x, 0, transform.position.z - from.position.z);
//        force = force.normalized;
//        force.y = 2;
//        body.AddForce(force * power);
    }

    void SetAnimationState()
    {
        animations.SetBool(IsIdle, currentState == states.Idle);
        animations.SetBool(IsWalking, currentState == states.Walking);
        animations.SetBool(IsAttacking, currentState == states.Attacking);
        animations.SetBool(IsDeath, currentState == states.Death);
    }

    void SetNewIdleWalkPoint()
    {
        Vector3 value = Random.insideUnitSphere * 50;
        value += SpawnPoint.transform.position;
        NavMeshHit hit;
        if (NavMesh.SamplePosition(value, out hit, 50, 1))
        {
            agent.SetDestination(hit.position);
            currentState = states.Walking;
            // agent.isStopped = false;
        }
    }

    void OnDeath()
    {
        currentState = states.Death;
        SetAnimationState();
        GameManager.Instance.UnRegisterAsEnemy(gameObject);
        GetComponent<CapsuleCollider>().enabled = false;
        body.useGravity = false;
        agent.isStopped = true;
        agent.velocity = Vector3.zero;
        body.velocity = Vector3.zero;
        if (equipedItem != null)
        {
            if (equipedItem.Type == ItemController.ItemType.Key)
            {
                SpawnPoint.GetComponent<SkyBeamController>().UnRegisterSkeleton();
            }
            
            equipedItem.SetAsDropped();
            equipedItem = null;
        }

        GameManager.Instance.TotalSkeletonsKilled++;
    }
}