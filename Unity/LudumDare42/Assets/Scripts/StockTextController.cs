﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class StockTextController : MonoBehaviour
{
    private TextMeshPro text;
    private int lastCount = -1;

    private void Start()
    {
        text = GetComponentInChildren<TextMeshPro>();
    }

    private void FixedUpdate()
    {
        if (GameManager.Instance.TotalFood != lastCount)
        {
            lastCount = GameManager.Instance.TotalFood;
            text.text = @"Food storage
Drop more food here
Current stock " + lastCount;
        }
    }
}