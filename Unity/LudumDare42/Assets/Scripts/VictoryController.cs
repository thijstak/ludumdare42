﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class VictoryController : MonoBehaviour
{
    // Update is called once per frame
    void Update()
    {
        if (GameManager.Instance.IsGameOver())
        {
            SceneManager.LoadScene(2);
        }
    }
}