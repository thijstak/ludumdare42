﻿using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;

public class ZoneCameraController : MonoBehaviour
{
    public CinemachineVirtualCamera camera;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            camera.Priority = 100;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        camera.Priority = 0;
    }
}