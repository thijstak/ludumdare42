﻿using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;

public class ItemController : MonoBehaviour
{
    public enum ItemType
    {
        Food,
        Key
    }

    public ItemType Type = ItemType.Food;
    public int FoodValue = 1;
    private GameObject light;
    private Rigidbody body;
    private Collider collider;

    // Use this for initialization
    void Awake()
    {
        light = GetComponentInChildren<Light>().gameObject;
        body = GetComponent<Rigidbody>();
        collider = GetComponent<Collider>();
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.parent != null)
        {
            transform.position = transform.parent.position;
        }
    }

    public void SetAsCarried(Transform parent)
    {
        light.SetActive(false);
        body.useGravity = false;
        collider.enabled = false;
        body.velocity = Vector3.zero;
        body.angularVelocity = Vector3.zero;
        transform.parent = parent;
        transform.position = parent.position;
    }

    public void SetAsDropped()
    {
        transform.parent = null;
        light.SetActive(true);
        body.useGravity = true;
        collider.enabled = true;
    }
}