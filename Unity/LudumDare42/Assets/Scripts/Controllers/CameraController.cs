﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using Cinemachine;
using UnityEngine;
using Debug = UnityEngine.Debug;

public class CameraController : MonoBehaviour
{
    public CinemachineVirtualCamera Camera;
    public float RotationSpeed = 10;

    // Use this for initialization
    void Start()
    {
        if (Camera == null)
        {
            Camera = GetComponent<CinemachineVirtualCamera>();
        }
    }

    // Update is called once per frame
    void Update()
    {
        UpdateMovement();
    }

    void UpdateMovement()
    {
        float mouseMovement = Input.GetAxis("Mouse X");
        if (mouseMovement != 0)
        {
            Camera.transform.Translate(Vector3.right * mouseMovement * RotationSpeed);
        }
    }
}