﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Linq;
using System.Threading;
using Cinemachine;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Experimental.UIElements;

public class PlayerController : MonoBehaviour
{
    public Camera mainCamera;
    public CinemachineVirtualCamera FollowCamera;
    public CinemachineVirtualCamera CombatCamera;
    public CinemachineTargetGroup TargetGroup;
    public CinemachineVirtualCamera HomeCamera;
    public GameObject AttackRange;
    public GameObject AttackStart;

    public float MovementSpeed = 3;
    public float CombatMovementSpeed = 2;
    public float JumpPower = 10;
    public float LockonDistance = 50.0f;
    public float PickupRange = 10.0f;

    public int SwordDamage = 1;
    public float SwordPower = 500;

    public float JumpDelay = 0.5f;
    public float LandDistance = 0.4f;
    public float EquipTimer = 1f;
    public float AttackTimer = 0.25f;

    public Transform ItemSlot1Location;
    public GameObject ItemSlot1Item;

    public Transform ItemSlot2Location;
    public GameObject ItemSlot2Item;

    public Transform ItemSlot3Location;
    public GameObject ItemSlot3Item;
    public bool ItemSlot3Unlocked;

    public Transform ItemSlot4Location;
    public GameObject ItemSlot4Item;
    public bool ItemSlot4Unlocked;

    public Transform ItemSlot5Location;
    public GameObject ItemSlot5Item;
    public bool ItemSlot5Unlocked;

    public Transform ItemSlot6Location;
    public GameObject ItemSlot6Item;
    public bool ItemSlot6Unlocked;

    public GameObject[] EquipmentSheeted;
    public GameObject[] EquipmentEquiped;

    private Animator animator;
    private Rigidbody rb;

    private bool isJumping = false;
    private bool isGrounded = true;
    private bool weaponsEquiped = false;

    private const string IsRunning = "IsRunning";
    private const string IsJumping = "IsJumping";
    private const string HorizontalSpeed = "HorizontalSpeed";
    private const string VerticalSpeed = "VerticalSpeed";
    private const string IsFalling = "IsFalling";
    private const string IsLockedOn = "IsLockedOn";
    private const string IsAttacking = "IsAttacking";
    private const string CombatMoveLeft = "CombatLeft";
    private const string CombatMoveRight = "CombatRight";
    private const string CombatForward = "CombatForward";

    private float jumpTimer = 0.0f;
    private float equipTimer = 0.0f;
    private float attackTimer = 0.0f;

    private GameObject lockOnTarget = null;

    // Use this for initialization
    void Start()
    {
        animator = GetComponentInChildren<Animator>();
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        jumpTimer -= Time.deltaTime;
        equipTimer -= Time.deltaTime;
        attackTimer -= Time.deltaTime;
    }


    // Update is called once per frame
    void FixedUpdate()
    {
        UpdateInput();
    }

    void UpdateInput()
    {
        // Check default actions.
        CheckLock();

        // Check if we are in combat mode.
        if (lockOnTarget != null)
        {
            // Combat mode checks.
            GameManager.Instance.HighlightKeys = false;
            CheckAttack();
            CheckMovementCombat();
        }
        else
        {
            // Non combat checks.
            ResetCombatAnimationState();
            ClearLockon();

            CheckAction();
            CheckJump();
            CheckMovementNormal();
            CheckHomeCamera();
        }

        // Default check to perform after input.
        RaycastHit hit;
        if (Physics.Raycast(transform.position, Vector3.down, out hit) && rb.velocity.y < 0)
        {
            if (hit.distance <= LandDistance)
            {
                isJumping = false;
                animator.SetBool(IsJumping, false);
                animator.SetBool(IsFalling, false);
                isGrounded = true;
            }
            else
            {
                animator.SetBool(IsFalling, true);
                isGrounded = false;
            }
        }
    }

    private void CheckHomeCamera()
    {
        if (Input.GetButton("ShowHome"))
        {
            HomeCamera.Priority = 100;
        }
        else
        {
            HomeCamera.Priority = 0;
        }
    }

    void CheckJump()
    {
        // Jump routine.
        if (isJumping || !isGrounded || !Input.GetButton("Jump") || !(jumpTimer <= 0)) return;

        isJumping = true;
        animator.SetBool(IsJumping, true);
        jumpTimer = JumpDelay;
        rb.AddForce(0, JumpPower, 0, ForceMode.VelocityChange);
    }

    void CheckEquip()
    {
//        // Equip routine.
//        if (isJumping || !isGrounded || !Input.GetButtonDown("Equip") || !(equipTimer <= 0)) return;
//
//        equipTimer = EquipTimer;
//        SwitchEquipement();
    }

    void CheckAttack()
    {
        if (lockOnTarget != null)
        {
            if (Input.GetButtonDown("Attack") && attackTimer <= 0)
            {
                attackTimer = AttackTimer;
                animator.SetBool(IsAttacking, true);

                var start = AttackStart.transform.position;
                var end = AttackRange.transform.position;

                float distance = Vector3.Distance(start, end);
                // Debug.Log("Distance: " + distance);
                // Debug.DrawLine(start, end, Color.magenta, 5);
                var hits = Physics.BoxCastAll(start, transform.localScale, transform.forward, Quaternion.identity,
                    distance);

                // Debug.Log("Found targets: " + hits.Length);
                foreach (RaycastHit hit in hits)
                {
                    // Debug.Log("Found target: " + hit.collider.gameObject.name);
                    if (hit.collider.gameObject.CompareTag("Enemy"))
                    {
                        SkeletonBehaviour sb = hit.collider.gameObject.GetComponent<SkeletonBehaviour>();
                        if (sb == null) return; // ?? Did I mess up somewhere????

                        sb.TakeDamage(transform, SwordDamage, SwordPower);
                    }
                }

                return;
            }
        }

        if (attackTimer < 0)
        {
            animator.SetBool(IsAttacking, false);
        }
    }

    void CheckLock()
    {
        if (!Input.GetButton("Lock") || (lockOnTarget != null &&
                                         Vector3.Distance(lockOnTarget.transform.position,
                                             gameObject.transform.position) > LockonDistance))
        {
            if (lockOnTarget != null)
            {
                lockOnTarget = null;
            }

            ClearLockon();
            return;
        }

        if (lockOnTarget == null)
        {
            lockOnTarget = GameManager.Instance.Enemies
                .Where(e => Vector3.Distance(e.transform.position, gameObject.transform.position) <= LockonDistance)
                .OrderBy(e => Vector3.Distance(e.transform.position, gameObject.transform.position) <= LockonDistance)
                .FirstOrDefault();

            if (lockOnTarget == null)
            {
                ClearLockon();
            }
            else
            {
                SetLockon();
            }
        }
    }

    void CheckAction()
    {
        if (!Input.GetButton("Attack"))
        {
            GameManager.Instance.HighlightKeys = false;
            return;
        }

        GameManager.Instance.HighlightKeys = true;

        RaycastHit[] hits = Physics.SphereCastAll(transform.position, PickupRange, transform.forward);

        GameObject item = null;
        foreach (RaycastHit hit in hits)
        {
            if (hit.collider.CompareTag("Item"))
            {
                item = hit.collider.gameObject;
                break;
            }
        }

        if (item == null) return;

        // The pickup action!!!
        // Get the first slot that is free.
        if (ItemSlot1Item == null)
        {
            PickupItem(item, ItemSlot1Location);
            ItemSlot1Item = item;
            return;
        }

        if (ItemSlot2Item == null)
        {
            PickupItem(item, ItemSlot2Location);
            ItemSlot2Item = item;
            return;
        }

        if (ItemSlot3Unlocked && ItemSlot3Item == null)
        {
            PickupItem(item, ItemSlot3Location);
            ItemSlot3Item = item;
            return;
        }

        if (ItemSlot4Unlocked && ItemSlot4Item == null)
        {
            PickupItem(item, ItemSlot4Location);
            ItemSlot4Item = item;
            return;
        }

        if (ItemSlot5Unlocked && ItemSlot5Item == null)
        {
            PickupItem(item, ItemSlot5Location);
            ItemSlot5Item = item;
            return;
        }

        if (ItemSlot6Unlocked && ItemSlot6Item == null)
        {
            PickupItem(item, ItemSlot6Location);
            ItemSlot6Item = item;
            return;
        }
    }

    void CheckMovementNormal()
    {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        Vector3 velocity = mainCamera.transform.TransformDirection(new Vector3(horizontal, 0, vertical)) *
                           MovementSpeed;

        if (velocity.magnitude != 0)
        {
            animator.SetBool(IsRunning, true);
            transform.rotation = Quaternion.LookRotation(velocity);
        }
        else
        {
            animator.SetBool(IsRunning, false);
        }

        animator.SetFloat(HorizontalSpeed, velocity.sqrMagnitude);

        velocity.y = rb.velocity.y;
        animator.SetFloat(VerticalSpeed, rb.velocity.y);
        rb.velocity = velocity;
    }

    void CheckMovementCombat()
    {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        if (lockOnTarget != null)
        {
            gameObject.transform.LookAt(new Vector3(lockOnTarget.transform.position.x, transform.position.y,
                lockOnTarget.transform.position.z));
        }

        if (attackTimer > 0)
        {
            return; // No movement in attack animation.
        }

        Vector3 velocity = transform.right * horizontal * CombatMovementSpeed;

        animator.SetBool(CombatMoveRight, horizontal > 0);
        animator.SetBool(CombatMoveLeft, horizontal < 0);

        velocity += transform.forward * vertical * CombatMovementSpeed;
        animator.SetBool(CombatForward, vertical != 0);

        velocity.y = rb.velocity.y;

        rb.velocity = velocity;
    }

    void ClearLockon()
    {
        CombatCamera.Priority = 0;
        animator.SetBool(IsLockedOn, false);
        SwitchEquipement(false);
    }

    void SetLockon()
    {
        CombatCamera.Priority = 100;
        animator.SetBool(IsLockedOn, true);
        TargetGroup.m_Targets[1].target = lockOnTarget.transform;
        SwitchEquipement(true);
        DropItem1And2();
    }

    void ResetCombatAnimationState()
    {
        animator.SetBool(CombatMoveRight, false);
        animator.SetBool(CombatMoveLeft, false);
        animator.SetBool(CombatForward, false);
        animator.SetBool(IsAttacking, false);
    }

    void SwitchEquipement(bool equip)
    {
        foreach (GameObject o in EquipmentSheeted)
        {
            o.SetActive(!equip);
        }

        foreach (GameObject o in EquipmentEquiped)
        {
            o.SetActive(equip);
        }

        weaponsEquiped = equip;
    }

    void PickupItem(GameObject item, Transform location)
    {
        item.GetComponent<ItemController>().SetAsCarried(location);
    }

    void DropItem1And2()
    {
        if (ItemSlot1Item != null)
        {
            ItemSlot1Item.GetComponent<ItemController>().SetAsDropped();
            ItemSlot1Item = null;
        }

        if (ItemSlot2Item != null)
        {
            ItemSlot2Item.GetComponent<ItemController>().SetAsDropped();
            ItemSlot2Item = null;
        }
    }

    public int GetCarriedOfType(ItemController.ItemType type)
    {
        return GetEquipedItems().Count(i => i.Type == type);
    }

    public void DestroyAllOfType(ItemController.ItemType type)
    {
        if (ItemSlot1Item != null)
        {
            var component = ItemSlot1Item.GetComponent<ItemController>();
            if (component.Type == type)
            {
                GameObject.Destroy(ItemSlot1Item);
                ItemSlot1Item = null;
            }
        }

        if (ItemSlot2Item != null)
        {
            var component = ItemSlot2Item.GetComponent<ItemController>();
            if (component.Type == type)
            {
                GameObject.Destroy(ItemSlot2Item);
                ItemSlot2Item = null;
            }
        }

        if (ItemSlot3Item != null)
        {
            var component = ItemSlot3Item.GetComponent<ItemController>();
            if (component.Type == type)
            {
                GameObject.Destroy(ItemSlot3Item);
                ItemSlot3Item = null;
            }
        }

        if (ItemSlot4Item != null)
        {
            var component = ItemSlot4Item.GetComponent<ItemController>();
            if (component.Type == type)
            {
                GameObject.Destroy(ItemSlot4Item);
                ItemSlot4Item = null;
            }
        }

        if (ItemSlot5Item != null)
        {
            var component = ItemSlot5Item.GetComponent<ItemController>();
            if (component.Type == type)
            {
                GameObject.Destroy(ItemSlot5Item);
                ItemSlot5Item = null;
            }
        }

        if (ItemSlot6Item != null)
        {
            var component = ItemSlot6Item.GetComponent<ItemController>();
            if (component.Type == type)
            {
                GameObject.Destroy(ItemSlot6Item);
                ItemSlot6Item = null;
            }
        }
    }

    IEnumerable<ItemController> GetEquipedItems()
    {
        List<ItemController> items = new List<ItemController>();

        if (ItemSlot1Item != null)
        {
            items.Add(ItemSlot1Item.GetComponent<ItemController>());
        }

        if (ItemSlot2Item != null)
        {
            items.Add(ItemSlot2Item.GetComponent<ItemController>());
        }

        if (ItemSlot3Item != null)
        {
            items.Add(ItemSlot3Item.GetComponent<ItemController>());
        }

        if (ItemSlot4Item != null)
        {
            items.Add(ItemSlot4Item.GetComponent<ItemController>());
        }

        if (ItemSlot5Item != null)
        {
            items.Add(ItemSlot5Item.GetComponent<ItemController>());
        }

        if (ItemSlot6Item != null)
        {
            items.Add(ItemSlot6Item.GetComponent<ItemController>());
        }

        return items;
    }

    public void UpgradeStorage()
    {
        if (!ItemSlot3Unlocked)
        {
            ItemSlot3Unlocked = true;
            return;
        }
        
        if (!ItemSlot4Unlocked)
        {
            ItemSlot4Unlocked = true;
            return;
        }
        
        if (!ItemSlot5Unlocked)
        {
            ItemSlot5Unlocked = true;
            return;
        }
        
        if (!ItemSlot6Unlocked)
        {
            ItemSlot6Unlocked = true;
        }
    }
}