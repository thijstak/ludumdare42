﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkeletonSpawner : MonoBehaviour
{
    public GameObject[] SkeletonsToSpawn = null;
    public GameObject Player;
    public float PlayerWithinRangeToSpawn = 100f;
    private bool hasSpawned = false;

    void Start()
    {
        Player = GameObject.FindWithTag("Player");

        if (SkeletonsToSpawn == null)
        {
            hasSpawned = true;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (hasSpawned) return;

        if (Vector3.Distance(Player.transform.position, transform.position) <= PlayerWithinRangeToSpawn)
        {
            foreach (GameObject o in SkeletonsToSpawn)
            {
                GameObject.Instantiate(o, transform.position, transform.rotation);
            }

            hasSpawned = true;
        }
    }
}