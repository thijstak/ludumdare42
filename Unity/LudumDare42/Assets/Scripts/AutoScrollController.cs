﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.UIElements;
using UnityEngine.SceneManagement;

public class AutoScrollController : MonoBehaviour
{
    public Vector3 MovementDirection;
    public bool FastSpeed = false;
    public float SpeedMultiplier;

    // Update is called once per frame
    void Update()
    {
        gameObject.transform.position += MovementDirection * Time.deltaTime * (FastSpeed ? SpeedMultiplier : 1);
        
        if (Input.anyKey)
        {
            FastSpeed = true;
        }
        else
        {
            FastSpeed = false;
        }
    }
}