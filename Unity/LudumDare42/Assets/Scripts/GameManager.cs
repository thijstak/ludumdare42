﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance { get; private set; }

    public LinkedList<GameObject> Enemies { get; private set; }

    public bool HighlightKeys { get; set; }

    // Total food in stock.
    public int TotalFood { get; set; }

    public int TotalKeysFound { get; set; }
    public int TotalKeysInLevel { get; set; }

    public int TotalFoodFound { get; set; }
    public int TotalSkeletonsKilled { get; set; }
    public bool IsDeath { get; set; }

    public int PlayerStorageUnlocked { get; set; }
    public int PlayerWeaponUpgraded { get; set; }

    private void Awake()
    {
        if (Instance != null)
        {
            GameObject.Destroy(Instance);
        }

        Instance = this;
        Enemies = new LinkedList<GameObject>();
    }

    public void RegisterAsEnemy(GameObject enemy)
    {
        Enemies.AddLast(enemy);
    }

    public void UnRegisterAsEnemy(GameObject enemy)
    {
        Enemies.Remove(enemy);
    }

    public void RegisterKey()
    {
        TotalKeysInLevel++;
    }

    public bool IsGameOver()
    {
        return IsDeath || TotalKeysFound == TotalKeysInLevel;
    }
}