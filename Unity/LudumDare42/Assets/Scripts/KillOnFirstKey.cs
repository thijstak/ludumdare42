﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillOnFirstKey : MonoBehaviour
{
    private float timer = 25f;
    
    // Update is called once per frame
    void Update()
    {
        if (GameManager.Instance.TotalKeysFound > 0)
        {
            timer -= Time.deltaTime;
            if (timer <= 0)
            {
                GameObject.Destroy(gameObject);               
            }
        }
    }
}