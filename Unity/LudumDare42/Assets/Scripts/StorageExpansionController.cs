﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class StorageExpansionController : MonoBehaviour
{
    public int[] FoodPerLevel = new int[]
    {
        4, 7, 12, 20
    };

    int CurrentLevel = -1;
    TextMeshPro text;
    private bool inZone = false;
    private GameObject player;

    private void Start()
    {
        text = GetComponentInChildren<TextMeshPro>();
        player = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.Instance.PlayerStorageUnlocked != CurrentLevel)
        {
            CurrentLevel = GameManager.Instance.PlayerStorageUnlocked;
            if (CurrentLevel == 4)
            {
                text.text = @"Upgrade storage here

Max capacity reached";
            }
            else
            {
                text.text = string.Format(@"Upgrade storage here
Current capacity:{0}
Next upgrade cost:{1}", CurrentLevel + 2, FoodPerLevel[CurrentLevel]);                
            }
        }

        if (inZone && CurrentLevel < 4 && Input.GetButtonDown("Attack"))
        {
            if (GameManager.Instance.TotalFood >= FoodPerLevel[CurrentLevel])
            {
                GameManager.Instance.TotalFood -= FoodPerLevel[CurrentLevel];
                GameManager.Instance.PlayerStorageUnlocked++;
                player.GetComponent<PlayerController>().UpgradeStorage();
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            inZone = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            inZone = false;
        }
    }
}