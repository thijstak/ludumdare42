﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class SmithController : MonoBehaviour {

	public int[] FoodPerLevel = new int[]
	{
		2, 8, 20
	};

	int CurrentLevel = -1;
	TextMeshPro text;
	private bool inZone = false;
	private GameObject player;

	private void Start()
	{
		text = GetComponentInChildren<TextMeshPro>();
		player = GameObject.FindGameObjectWithTag("Player");
	}

	// Update is called once per frame
	void Update()
	{
		if (GameManager.Instance.PlayerWeaponUpgraded != CurrentLevel)
		{
			CurrentLevel = GameManager.Instance.PlayerWeaponUpgraded;
			if (CurrentLevel == 3)
			{
				text.text = @"Upgrade weapon here

Max damage reached";
			}
			else
			{
				text.text = string.Format(@"Upgrade sword here
Current damage:{0}
Next upgrade cost:{1}", CurrentLevel + 1, FoodPerLevel[CurrentLevel]);                
			}
		}

		if (inZone && CurrentLevel < 3 && Input.GetButtonDown("Attack"))
		{
			if (GameManager.Instance.TotalFood >= FoodPerLevel[CurrentLevel])
			{
				GameManager.Instance.TotalFood -= FoodPerLevel[CurrentLevel];
				GameManager.Instance.PlayerWeaponUpgraded++;
				player.GetComponent<PlayerController>().SwordDamage++;
			}
		}
	}
	
	private void OnTriggerEnter(Collider other)
	{
		if (other.CompareTag("Player"))
		{
			inZone = true;
		}
	}

	private void OnTriggerExit(Collider other)
	{
		if (other.CompareTag("Player"))
		{
			inZone = false;
		}
	}
}
