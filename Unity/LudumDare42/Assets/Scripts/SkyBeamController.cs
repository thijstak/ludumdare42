﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkyBeamController : MonoBehaviour
{
    // Living?
    private int noOfLivingSkeletons = 0;
    public GameObject beam;

    // Update is called once per frame
    void Update()
    {
        beam.SetActive(GameManager.Instance.HighlightKeys && noOfLivingSkeletons > 0);
    }

    public void RegisterSkeleton()
    {
        noOfLivingSkeletons++;
    }

    public void UnRegisterSkeleton()
    {
        noOfLivingSkeletons--;
    }
}