﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class EndScreenScript : MonoBehaviour
{
    // Use this for initialization
    void Start()
    {
        var i = GameManager.Instance;

        string t = string.Format(@"
Congratulations!

{0}

In the process you have managed to kill {1} innocent skeletons and pilfered {2} food from them.
", i.IsDeath ? GetDeathText() : GetVictoryText(), i.TotalSkeletonsKilled, i.TotalFoodFound);

        GetComponentInChildren<TextMeshPro>().text = t;
    }

    string GetDeathText()
    {
        return @"


""Here ends the story of our brave hero. He could not find all the keys to his home and managed to get himselve killed in the process.
We will forever remember him as Hero the evicted. The skeletons of the field will hold parties in his home every day to celebrate.""




";
    }

    string GetVictoryText()
    {
        return @"


""Valiantly you have braved the dangers of the fields and reclaimed the keys to your home. The thieving hands of the skeletons will bother you no more.


Untill two week later, when you managed to 'loose' your keys again...""




";
    }
}