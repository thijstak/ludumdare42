﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class KeyZoneController : MonoBehaviour
{
    private TextMeshPro text;
    private int currentKeys;
    private int totalKeys;

    // Use this for initialization
    void Start()
    {
        text = GetComponentInChildren<TextMeshPro>();
    }

    // Update is called once per frame
    void Update()
    {
        if (currentKeys != GameManager.Instance.TotalKeysFound || totalKeys != GameManager.Instance.TotalKeysInLevel)
        {
            currentKeys = GameManager.Instance.TotalKeysFound;
            totalKeys = GameManager.Instance.TotalKeysInLevel;

            text.text = string.Format(@"Key storage
{0} of {1} keys found.
Find all keys to win.", currentKeys, totalKeys);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            PlayerController pc = other.gameObject.GetComponent<PlayerController>();
            int numberOfKeys = pc.GetCarriedOfType(ItemController.ItemType.Key);
            pc.DestroyAllOfType(ItemController.ItemType.Key);
            GameManager.Instance.TotalKeysFound += numberOfKeys;
        }
    }
}