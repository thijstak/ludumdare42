﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RegisterEnemy : MonoBehaviour
{
    // Use this for initialization
    void Start()
    {
        GameManager.Instance.RegisterAsEnemy(gameObject);
    }
}