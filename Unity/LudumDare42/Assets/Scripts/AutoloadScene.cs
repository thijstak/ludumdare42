﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AutoloadScene : MonoBehaviour
{
    public float YReached;
    public int SceneNumber = 1;

    // Update is called once per frame
    void Update()
    {
        if (transform.position.y >= YReached)
        {
            SceneManager.LoadScene(SceneNumber);
        }
        
        if (Input.GetButton("Submit"))
        {
            SceneManager.LoadScene(SceneNumber);
        }
    }
}