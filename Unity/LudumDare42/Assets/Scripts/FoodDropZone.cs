﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoodDropZone : MonoBehaviour
{
    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag("Player")) return;

        PlayerController playerController = other.gameObject.GetComponent<PlayerController>();
        if (playerController == null) return; // Testing issue.

        int totalFood = playerController.GetCarriedOfType(ItemController.ItemType.Food);
        GameManager.Instance.TotalFood += totalFood;
        GameManager.Instance.TotalFoodFound += totalFood;
        playerController.DestroyAllOfType(ItemController.ItemType.Food);
    }
}